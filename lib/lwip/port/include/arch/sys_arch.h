#ifndef LWIP_ARCH_SYS_ARCH_H
#define LWIP_ARCH_SYS_ARCH_H

#include "lwip/opt.h"
#include "lwip/arch.h"
#include "lwip/err.h"

#define ERR_NEED_SCHED 123

void sys_arch_msleep(u32_t delay_ms);
#define sys_msleep(ms) sys_arch_msleep(ms)

#if SYS_LIGHTWEIGHT_PROT
typedef u32_t sys_prot_t;
#endif /* SYS_LIGHTWEIGHT_PROT */

#include <errno.h>

#define SYS_MBOX_NULL NULL
#define SYS_SEM_NULL  NULL

typedef u32_t sys_prot_t;

struct sys_sem;
typedef struct sys_sem * sys_sem_t;
#define sys_sem_valid(sem) (((sem) != NULL) && (*(sem) != NULL))
#define sys_sem_set_invalid(sem) do { if((sem) != NULL) { *(sem) = NULL; }}while(0)

/* let sys.h use binary semaphores for mutexes */
#define LWIP_COMPAT_MUTEX 1
#define LWIP_COMPAT_MUTEX_ALLOWED 1

struct sys_mbox;
typedef struct sys_mbox *sys_mbox_t;
#define sys_mbox_valid(mbox) (((mbox) != NULL) && (*(mbox) != NULL))
#define sys_mbox_set_invalid(mbox) do { if((mbox) != NULL) { *(mbox) = NULL; }}while(0)

struct sys_thread;
typedef struct sys_thread * sys_thread_t;

static inline u32_t sys_arch_sem_wait(sys_sem_t *sem, u32_t timeout)
{
	return 0;
};

static inline err_t sys_mbox_trypost(sys_mbox_t *mbox, void *msg)
{
      	return 0;
};

#if 0
#define sys_sem_new(s, c) ERR_OK

#define sys_sem_wait(s)
#define sys_sem_free(s)
#define sys_sem_valid(s) 0
#define sys_sem_set_invalid(s)
#define sys_mutex_new(mu) ERR_OK
#define sys_mutex_lock(mu)
#define sys_mutex_unlock(mu)
#define sys_mutex_free(mu)
#define sys_mutex_valid(mu) 0
#define sys_mutex_set_invalid(mu)
#define sys_mbox_new(m, s) ERR_OK
#define sys_mbox_fetch(m,d)
#define sys_mbox_tryfetch(m,d)
#define sys_mbox_post(m,d)

#define sys_mbox_free(m)
#define sys_mbox_valid(m)
#define sys_mbox_set_invalid(m)
#endif

#define sys_sem_signal(s)

int uboot_lwip_init(void);

#endif /* LWIP_ARCH_SYS_ARCH_H */
