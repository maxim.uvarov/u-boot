#include <common.h>
#include <command.h>

#include "lwip/debug.h"
#include "lwip/arch.h"
#include "netif/etharp.h"
#include "lwip/stats.h"
#include "lwip/def.h"
#include "lwip/mem.h"
#include "lwip/pbuf.h"
#include "lwip/sys.h"

#include "lwip/ip.h"

#define IFNAME0 'e'
#define IFNAME1 '0'

struct uboot_lwip_if {
};

static struct netif uboot_netif;

#define LWIP_PORT_INIT_IPADDR(addr)   IP4_ADDR((addr), 192,168,1,200)
#define LWIP_PORT_INIT_GW(addr)       IP4_ADDR((addr), 192,168,1,1)
#define LWIP_PORT_INIT_NETMASK(addr)  IP4_ADDR((addr), 255,255,255,0)

#if 0
 /* TBD: to u-boot input */
static struct pbuf * low_level_input(struct netif *netif)
{
	struct ethernetif *ethernetif = netif->state;
	struct pbuf *p, *q;
	u16_t len;

	/* Obtain the size of the packet and put it into the "len"
	   variable. */
	len = ;

#if ETH_PAD_SIZE
	len += ETH_PAD_SIZE; /* allow room for Ethernet padding */
#endif

	/* We allocate a pbuf chain of pbufs from the pool. */
	p = pbuf_alloc(PBUF_RAW, len, PBUF_POOL);

	if (p != NULL) {

#if ETH_PAD_SIZE
		pbuf_remove_header(p, ETH_PAD_SIZE); /* drop the padding word */
#endif

		/* We allocate a pbuf chain of pbufs from the pool. */
		p = pbuf_alloc(PBUF_RAW, len, PBUF_POOL);

		if (p != NULL) {

#if ETH_PAD_SIZE
			pbuf_remove_header(p, ETH_PAD_SIZE); /* drop the padding word */
#endif

			/* We iterate over the pbuf chain until we have read the entire
			 * packet into the pbuf. */
			for (q = p; q != NULL; q = q->next) {
				/* Read enough bytes to fill this pbuf in the chain. The
				 * available data in the pbuf is given by the q->len
				 * variable.
				 * This does not necessarily have to be a memcpy, you can also preallocate
				 * pbufs for a DMA-enabled MAC and after receiving truncate it to the
				 * actually received size. In this case, ensure the tot_len member of the
				 * pbuf is the sum of the chained pbuf len members.
				 */
				read data into(q->payload, q->len);
			}
			acknowledge that packet has been read();

#if ETH_PAD_SIZE
			pbuf_add_header(p, ETH_PAD_SIZE); /* reclaim the padding word */
#endif

			LINK_STATS_INC(link.recv);
		} else {
			drop packet();
			LINK_STATS_INC(link.memerr);
			LINK_STATS_INC(link.drop);
		}

		return p;
}
#endif

/* Maxim: accoding to lwip idea this has to be separate thread in the background
 * to poll RX packets from the network. The first we need to try to inject
 * it directly to net/net.c eth_rx() calls. Assuming we do not implement threading
 * and we roll in the main polling loop. Then socket operation should also be possible
 * to call for non blocking socket calls.
 * If lwip will not work without threading, then we to implement some scheduler with semaphores.
 * We can base ./examples/standalone/sched.c example code for that.
 *
 */
static int ethernetif_input(struct pbuf *p, struct netif *netif)
{
#if 0
	struct ethernetif *ethernetif;
	//struct eth_hdr *ethhdr;
	struct pbuf *p;

	ethernetif = netif->state;

	/* move received packet into a new pbuf */
	//p = low_level_input(netif);

	/* if no packet could be read, silently ignore this */
	if (p != NULL) {
		/* pass all packets to ethernet_input, which decides what packets it supports */
		if (netif->input(p, netif) != ERR_OK) {
			LWIP_DEBUGF(NETIF_DEBUG, ("ethernetif_input: IP input error\n"));
			pbuf_free(p);
			p = NULL;
		}
	}
#endif
	return 0;
}

static err_t low_level_output(struct netif *netif, struct pbuf *p)
{
	/* TBD */

        printf("TX data len %d\n", p->tot_len);
	return ERR_OK;
}

err_t uboot_lwip_if_init(struct netif *netif)
{
	struct uboot_lwip_if *uif = (struct uboot_lwip_if *)mem_malloc(sizeof(struct uboot_lwip_if));

	if (uif == NULL) {
		LWIP_DEBUGF(NETIF_DEBUG, ("if_init: out of memory for uif\n"));
		return ERR_MEM;
	}
	netif->state = uif;

	netif->name[0] = IFNAME0;
	netif->name[1] = IFNAME1;
#if LWIP_IPV4
	netif->output = etharp_output;
#endif /* LWIP_IPV4 */
#if LWIP_IPV6
	netif->output_ip6 = ethip6_output;
#endif /* LWIP_IPV6 */
	netif->linkoutput = low_level_output;
	netif->mtu = 1500;

	printf("Initialized LWIP stack\n");
	return ERR_OK;
}

int uboot_lwip_init()
{
	ip4_addr_t ipaddr, netmask, gw;
	//int err;

	ip4_addr_set_zero(&gw);
	ip4_addr_set_zero(&ipaddr);
	ip4_addr_set_zero(&netmask);

#if USE_DHCP
	printf("Starting lwIP, local interface IP is dhcp-enabled\n");
#elif USE_AUTOIP
	printf("Starting lwIP, local interface IP is autoip-enabled\n");
#else /* USE_DHCP */
	LWIP_PORT_INIT_GW(&gw);
	LWIP_PORT_INIT_IPADDR(&ipaddr);
	LWIP_PORT_INIT_NETMASK(&netmask);
	printf("Starting lwIP, local interface IP is %s\n", ip4addr_ntoa(&ipaddr));
#endif /* USE_DHCP */

	netif_add(&uboot_netif, &ipaddr, &netmask, &gw, &uboot_netif, uboot_lwip_if_init, ethernetif_input);

#if LWIP_IPV6
	netif_create_ip6_linklocal_address(netif_default, 1);
	printf("ip6 linklocal address: %s\n", ip6addr_ntoa(netif_ip6_addr(netif_default, 0)));
#endif /* LWIP_IPV6 */

#if LWIP_NETIF_STATUS_CALLBACK
	netif_set_status_callback(netif_default, status_callback);
#endif /* LWIP_NETIF_STATUS_CALLBACK */
#if LWIP_NETIF_LINK_CALLBACK
	netif_set_link_callback(netif_default, link_callback);
#endif /* LWIP_NETIF_LINK_CALLBACK */

#if LWIP_AUTOIP
	autoip_set_struct(netif_default, &netif_autoip);
#endif /* LWIP_AUTOIP */
#if LWIP_DHCP
	dhcp_set_struct(netif_default, &netif_dhcp);
#endif /* LWIP_DHCP */
	netif_set_up(netif_default);
#if USE_DHCP
	err = dhcp_start(netif_default);
	LWIP_ASSERT("dhcp_start failed", err == ERR_OK);
#elif USE_AUTOIP
	err = autoip_start(netif_default);
	LWIP_ASSERT("autoip_start failed", err == ERR_OK);
#endif /* USE_DHCP */

	return CMD_RET_SUCCESS;
}
