// SPDX-License-Identifier: GPL-2.0+
/*
 * (C) Copyright 2023
 * Maxim Uvarov, maxim.uvarov@linaro.org
 */

#include <common.h>
#include <command.h>
#include <console.h>
#include <display_options.h>
#include <memalign.h>

extern int uboot_lwip_init(void);

static int do_lwip_info(struct cmd_tbl *cmdtp, int flag, int argc,
		      char *const argv[])
{
	printf("TBD: %s\n", __func__);
	return CMD_RET_SUCCESS;
}

static int do_lwip_init(struct cmd_tbl *cmdtp, int flag, int argc,
		      char *const argv[])
{
	int ret;
	ret = uboot_lwip_init();
	printf("cmd: lwip init, ret %d\n", ret);
	return CMD_RET_SUCCESS;
}

static struct cmd_tbl cmds[] = {
	U_BOOT_CMD_MKENT(info, 1, 0, do_lwip_info, "", ""),
	U_BOOT_CMD_MKENT(init, 1, 0, do_lwip_init, "", ""),
};

static int do_ops(struct cmd_tbl *cmdtp, int flag, int argc,
		     char *const argv[])
{
	struct cmd_tbl *cp;

	cp = find_cmd_tbl(argv[1], cmds, ARRAY_SIZE(cmds));

	/* Drop the mmc command */
	argc--;
	argv++;

	if (cp == NULL || argc > cp->maxargs)
		return CMD_RET_USAGE;
	if (flag == CMD_FLAG_REPEAT && !cmd_is_repeatable(cp))
		return CMD_RET_SUCCESS;

	return cp->cmd(cmdtp, flag, argc, argv);
}

U_BOOT_CMD(
	lwip, 2, 1, do_ops,
	"LWIP sub system",
	"info - display info\n"
	"init - init LWIP\n"
	);

/* Old command kept for compatibility. Same as 'mmc info' */
U_BOOT_CMD(
	lwipinfo, 1, 0, do_lwip_info,
	"display LWIP info",
	"- display LWIP stack info"
);
